<?php
/**
 * Aliases for special pages
 *
 * @file
 * @ingroup Extensions
 */
// @codingStandardsIgnoreFile

$specialPageAliases = array();

/** English (English) */
$specialPageAliases['en'] = array(
	'NukeDPL' => array( 'NukeDPL', 'Nuke DPL' ),
);

/** German (Deutsch) */
$specialPageAliases['de'] = array(
	'NukeDPL' => array( 'Massenlöschung_DPL' ),
);

/** Zazaki (Zazaki) */
$specialPageAliases['diq'] = array(
	'NukeDPL' => array( 'PıperneDPL', 'Pıperne_DPL' ),
);

/** Simplified Chinese (中文（简体）‎) */
$specialPageAliases['zh-hans'] = array(
	'NukeDPL' => array( '大量删除DPL' ),
);

/** Traditional Chinese (中文（繁體）‎) */
$specialPageAliases['zh-hant'] = array(
	'NukeDPL' => array( '大量刪除DPL' ),
);